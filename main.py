import pycallgraph
import pycallgraph.output.graphviz
from rsa import RSA
from modulo import Modulo
# profiler1 = pycallgraph.PyCallGraph(output=pycallgraph.output.graphviz.GraphvizOutput(output_file='rsa_generating.png'))
# profiler1.start()
# rsa = RSA()
# rsa.generate_keys()
##2nd profile
# profiler2 = pycallgraph.PyCallGraph(output=pycallgraph.output.graphviz.GraphvizOutput(output_file='rsa_encrypting.png'))
# profiler2.start()
# profiler2.done()



Alice = RSA()
Bob = RSA()
modulus_length = 24
N, euler = False, False
while not N:
    p1, q1 = RSA.generate_p_q(modulus_length//2)
    p2, q2 = RSA.generate_p_q(modulus_length//2)
    N, euler = RSA.compute_commons((p1, q1), (p2, q2))
Alice.generate_keys_for_given(N, euler)
Bob.generate_keys_for_given(N, euler)


assert Alice.encrypt(Bob.encrypt(5)) == Bob.encrypt(Alice.encrypt(5))
assert Alice.decrypt(Bob.decrypt(Alice.encrypt(Bob.encrypt(5)))) == 5
assert Bob.decrypt(Alice.decrypt(Bob.encrypt(Alice.encrypt(5)))) == 5

B = [x for x in Bob.decrypt_stream(Alice.decrypt_stream(Bob.encrypt_stream(Alice.encrypt_stream(b'abc'))))]
A = [x for x in Alice.decrypt_stream(Bob.decrypt_stream(Alice.encrypt_stream(Bob.encrypt_stream(b'abc'))))]
C = [x for x in Bob.decrypt_stream(Alice.decrypt_stream(Alice.encrypt_stream(Bob.encrypt_stream(b'abc'))))]

assert A == B == C

X = Alice.sign(56)
assert Alice.verify(56, X)

#profiler1.done()

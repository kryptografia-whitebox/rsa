'''
    File name: modulo.py
    Author: Krzysztof Galus
    Date created: 10/28/2020
    Date last modified: 12/6/2020
    Python Version: 3.8.5
    Purpose: class for modular arithmetics
'''

__all__ = ['RSA']
__author__ = "Krzysztof Galus"
__license__ = "Public domain"
__email__ = "krzysztof.galus.k7@gmail.com"

from modulo import Modulo, naive_is_prime
from math import sqrt, floor, ceil, gcd
from random import randint, getrandbits, randrange


class RSA:
    def __init__(self, bit_size=64, public_key=None, private_key=None):
        self.public_key = public_key
        self.private_key = private_key
        self.bit_size = bit_size

    @staticmethod
    def euler_function(*args):
        result = 1
        for arg in args:
            try:
                if arg & 1:
                    result *= (arg.value - 1)
                else:
                    temp = 0
                    for i in range(1, arg.value-1):
                        if gcd(arg.value, i) == 1:
                            temp += 1
                    result += temp
            except (TypeError, AttributeError):
                if arg & 1:
                    result *= (arg - 1)
                else:
                    temp = 0
                    for i in range(1, arg-1):
                        if gcd(arg, i) == 1:
                            temp += 1
                    result += temp
        return result

    @staticmethod
    def check_if_prime(arg, acurracy=4):
        if arg == 2:         # 2 is a prime number
            return True

        if not (arg & 1):
            return False

        i = 0
        max_exponent = 0
        while((arg - 1) >= 2 ** i):          # representing (n - 1) as 2^r * d
            if (arg - 1) % 2 ** i == 0:
                max_exponent = i
            i += 1
        temp = [max_exponent, (arg - 1) // (2 ** max_exponent)]
        # k times pick a random integer 'a'
        for k in range(0, acurracy):
            # calculating x <- a^d mod n
            a = randint(1, (arg - 2))
            # if x == 1 or x == -1 then continue cause x is probably prime
            challenger = Modulo(arg, a) ** temp[1]
            if challenger == 1 or challenger == arg - 1:
                continue
            for i in range(temp[0]):                                    # r times
                # if x^2 mod n == -1 x is probably prime
                challenger = Modulo(arg, int(challenger) ** 2)
                if challenger == arg - 1:
                    break
            else:
                return False
        return True

    def generate_large_prime(self, bits):
        challenger = randint(2 ** (bits - 1) + 1, 2 ** bits - 1)
        if challenger % 2 == 0:
            challenger += 1
        while not self.check_if_prime(challenger):
            challenger += 2
        return challenger

    @staticmethod
    def generate_p_q(bits):
        p = randint(2 ** ((bits - 1)-1) + 1, 2 ** (bits-1) - 1)
        q = randint(2 ** ((bits - 1)-1) + 1, 2 ** (bits-1) - 1)
        return p, q

    @staticmethod
    def compute_commons(part_one, part_two):
        p1, q1 = part_one
        p2, q2 = part_two

        if not (RSA.check_if_prime(p1 + p2) and RSA.check_if_prime(q1 + q2)):
            return False, False

        N = (p1 + p2) * (q1 + q2)

        euler = RSA.euler_function(p1 + p2, q1 + q2)
        return N, euler

    def generate_keys_for_given(self, N, euler):
        e = randint(euler//4, euler)
        while gcd(e, euler) != 1:
            e = randint(euler//4, euler)
        d = Modulo(euler, e).inverse()

        self.private_key = Modulo(N, int(d))
        self.public_key = Modulo(N, e)

    def generate_keys(self):
        p, q = [self.generate_large_prime(self.bit_size) for i in range(2)]
        N = p * q
        euler = self.euler_function(p, q)
        e = randint(euler//4, euler)
        while gcd(e, euler) != 1:
            e = randint(euler//4, euler)
        d = Modulo(euler, e).inverse()

        self.public_key = Modulo(N, e)
        self.private_key = Modulo(N, int(d))

    def encrypt_stream(self, message):  #FIX (commutative version doesnt work)
        max_bit_length = (self.public_key.base).bit_length()                # taking max bit length of the public key
        assert max_bit_length >= 8
        decrypted = 0                                                       # encrypted -> message written to the varaible
        decrypted_length = 0                                                # encrypted_length -> length of written message in bytes
        B = (max_bit_length - 1)//8 + 1                                     # A and B -> A is a size(in bytes) of the single message taken 
        A = B #(max_bit_length - 8)//8                                         # B is a size of message which we send
        for part in message:                                                # iterating for each byte in message   
            decrypted <<= 8                                                 # shifting bits to the left, adding 1 to encrypted_length  
            decrypted_length += 1                                           # and adding part of the message to the variable 
            decrypted |= part                                               
            if decrypted_length >= A:
                encrypted = self.encrypt(decrypted)                         # encrypting message
                assert decrypted == self.decrypt(self.encrypt(decrypted))
                for n in reversed(range(B)):                                # sending each byte of message 
                    yield (encrypted >> (8*n)) & (2**8 - 1)
                decrypted = 0
                decrypted_length = 0
        else:                                                               # if every part of message was taken
            if decrypted_length:                                            # and there is unsent message cause it wasnt enough long
                while(decrypted_length < A):                                # we fill message with 0 to size A and send forward.
                    decrypted <<= 8
                    decrypted_length += 1
                encrypted = self.encrypt(decrypted)
                assert decrypted == self.decrypt(self.encrypt(decrypted))
                for n in reversed(range(B)):
                    yield (encrypted >> (8*n)) & (2**8 - 1)
                decrypted = 0
                decrypted_length = 0

    def decrypt_stream(self, message): #FIX (commutative version doesnt work)
        max_bit_length = (self.public_key.base).bit_length()                # taking max bit length of the public key 
        assert max_bit_length >= 8
        encrypted = 0                                                       # encrypted -> message written to the variable       
        encrypted_length = 0                                                # encrypted_length -> length of written message in bytes           
        B = (max_bit_length - 1)//8 + 1                                     # A and B -> B is a size(in bytes) of the single message taken     
        A = B #(max_bit_length - 8)//8                                         # A is a size of message which we send
        for part in message:                                                # iterating for each byte in message
            encrypted <<= 8                                                 # shifting bits to the left, adding 1 to encrypted_length       
            encrypted_length += 1                                           # and adding part of the message to the variable   
            encrypted |= part                                               #
            if encrypted_length == B:                                       # if message length is equal to size B
                decrypted = self.decrypt(encrypted)                         # decrypting message 
                for n in reversed(range(A)):                                # sending each byte of taken message
                    yield (decrypted >> 8*n) & (2**8 - 1)                   
                encrypted = 0
                encrypted_length = 0

    def encrypt(self, message):
        return int(Modulo(self.public_key.base, message) ** self.public_key.value)

    def decrypt(self, message):
        return int(Modulo(self.private_key.base, message) ** self.private_key.value)
    
    sign = decrypt

    def verify(self, message, signature):
        return self.encrypt(signature) == message

if __debug__ and __name__ == "__main__":
    # ASSERTS
    rsa = RSA(1024)
    rsa.generate_keys()
    assert rsa.euler_function(7, 11) == 60
    it = 0
    for i in range(100000):
        x = randint(10000, 200000)
        if rsa.check_if_prime(x) != naive_is_prime(x):
            it += 1
    assert it/100000 < 0.05
    for i in range(1000):
        assert naive_is_prime(rsa.generate_large_prime(16))

    prime_gaps = [
        [1	, 2],
        [2	, 3],
        [4	, 7],
        [6	, 23],
        [8	, 89],
        [14	, 113],
        [18	, 523],
        [20	, 887],
        [22	, 1129],
        [34	, 1327],
        [36	, 9551],
        [44	, 15683],
        [52	, 19609],
        [72	, 31397],
        [86	, 155921],
        [96	, 360653],
        [112,	370261],
        [114	, 492113],
        [118	, 1349533],
        [132	, 1357201],
        [148	, 2010733],
        [154	, 4652353],
        [180	, 17051707],
        [210	, 20831323],
        [220	, 47326693],
        [222	, 122164747],
        [234	, 189695659],
        [248	, 191912783],
        [250	, 387096133],
        [282	, 436273009],
        [288	, 1294268491],
        [292	, 1453168141],
        [320	, 2300942549],
        [336	, 3842610773],
        [354	, 4302407359],
        [382	, 10726904659],
        [384	, 20678048297],
        [394	, 22367084959],
        [456	, 25056082087],
        [464	, 42652618343],
        [468	, 127976334671],
        [474	, 182226896239],
        [486	, 241160624143],
        [490	, 297501075799],
        [500	, 303371455241],
        [514	, 304599508537],
        [516	, 416608695821],
        [532	, 461690510011],
        [534	, 614487453523],
        [540	, 738832927927],
        [582	, 1346294310749],
        [588	, 1408695493609],
        [602	, 1968188556461],
        [652	, 2614941710599],
        [674	, 7177162611713	],
        [716	, 13829048559701],
        [766	, 19581334192423],
        [778	, 42842283925351],
        [804	, 90874329411493],
        [806	, 171231342420521],
        [906	, 218209405436543],
        [916	, 1189459969825483],
        [924	, 1686994940955803],
        [1132	, 1693182318746371],
        [1184	, 43841547845541059],
        [1198	, 55350776431903243],
        [1220	, 80873624627234849],
        [1224	, 203986478517455989],
        [1248	, 218034721194214273],
        [1272	, 305405826521087869],
        [1328	, 352521223451364323],
        [1356	, 401429925999153707],
        [1370	, 418032645936712127],
        [1442	, 804212830686677669],
        [1476	, 1425172824437699411],
        [1488	, 5733241593241196731],
        [1510	, 6787988999657777797],
        [1526	, 15570628755536096243],
        [1530	, 17678654157568189057],
        [1550	, 18361375334787046697]]

    def prime_gap(arg):
        for element in prime_gaps:
            if element[1] >= arg:
                return element[0]
        return 10 ** 30

    for bits in range(56, 60):
        challenger = randint(2 ** (bits - 1) + 1, 2 ** bits - 1)
        counter = 0
        for i in range(10000):
            if rsa.check_if_prime(challenger):
                counter = 0
            assert counter <= prime_gap(
                challenger), f"{counter} {prime_gap(challenger)} {challenger}"
            counter += 1
            challenger += 1

    Alice = RSA()
    Bob = RSA()
    modulus_length = 64
    N, euler = False, False
    while not N:
        p1, q1 = RSA.generate_p_q(modulus_length//2)
        p2, q2 = RSA.generate_p_q(modulus_length//2)
        N, euler = RSA.compute_commons((p1, q1), (p2, q2))
    Alice.generate_key_for_given(N, euler)
    Bob.generate_key_for_given(N, euler)
    assert Alice.encrypt(Bob.encrypt(5)) == Bob.encrypt(Alice.encrypt(5))
    assert Alice.decrypt(Bob.decrypt(Alice.encrypt(Bob.encrypt(5)))) == 5
    assert Bob.decrypt(Alice.decrypt(Bob.encrypt(Alice.encrypt(7)))) == 7
